#define TAPE_SIZE 1024

#define OP_LESS_THAN '<'
#define OP_GREATER_THAN '>'
#define OP_PLUS '+'
#define OP_MINUS '-'
#define OP_DOT '.'
#define OP_COMMA ','
#define OP_LEFT_BRACKET '['
#define OP_RIGHT_BRACKET ']'

uint8_t tape[TAPE_SIZE];
uint16_t current_index = 0;
char buffer[2048];
uint16_t buffer_index = 0;
uint16_t buffer_end = 0;
uint16_t count = 0;

void setup(){
    for(int i = 0; i < TAPE_SIZE; i++){
      tape[i] = 0;
    }
    Serial.begin(9600);
    Serial.println("### Brainfuck interpreter ###");
}

void loop(){
    if(Serial.available()){
        char income = char(Serial.read());
        if(income == OP_COMMA || income == OP_DOT || income == OP_GREATER_THAN \
            || income == OP_LEFT_BRACKET || income == OP_LESS_THAN || income == OP_MINUS \
            || income == OP_PLUS || income == OP_RIGHT_BRACKET){
                //valid instruction
                buffer[buffer_end++] = income;
            }else if(income == '\n'){
                //ended
                while(buffer_index != buffer_end){
                    parse(buffer[buffer_index]);
                    buffer_index++;
                }
                buffer_end = 0;
                buffer_index = 0;
            }
    }

}

void parse(char cmd){

    // move to the left
    if(cmd == OP_LESS_THAN){ // <
        if(current_index > 0){
            current_index--;
        }
    }else
    // move to the right
    if(cmd == OP_GREATER_THAN){ // >
        if(current_index < TAPE_SIZE){
            current_index++;
        }
    }else
    // cell plus one
    if(cmd == OP_PLUS){ // +
        tape[current_index]++;
    }else
    // cell minus one
    if(cmd == OP_MINUS){ // -
        tape[current_index]--;
    }else
    // print cell
    if(cmd == OP_DOT){ // .
        uint8_t cell = tape[current_index];
        Serial.print("OUT [");
        Serial.print(current_index);
        Serial.print("]: ");
        if(cell >= 33 && cell <= 126){
          Serial.println(char(cell));
        }else{
          Serial.print("DEC ");
          Serial.println(cell);
        }
    }else
    // store input to cell
    if(cmd == OP_COMMA){ // ,
        Serial.print("IN [");
        Serial.print(current_index);
        Serial.print("]: ");
        tape[current_index] = uint8_t(Serial.read());
    }else
    // loop head
    if(cmd == OP_LEFT_BRACKET){ // [
        if(tape[current_index] == 0){
            buffer_index++;
            count = 0;
            while(buffer_index < buffer_end){
                if (buffer[buffer_index] == OP_RIGHT_BRACKET && count == 0){
                    break;
                }else if(buffer[buffer_index] == OP_LEFT_BRACKET){
                    count++;
                }else if(buffer[buffer_index] == OP_RIGHT_BRACKET){
                    count--;
                }
                buffer_index++;
            }
        }
    }else
    // loop tail
    if(cmd == OP_RIGHT_BRACKET){ // ]
        if(tape[current_index] != 0){
            count = 0;
            buffer_index--;
            while(buffer_index >= 0){
                if(buffer[buffer_index] == OP_LEFT_BRACKET && count == 0){
                    break;
                }else if(buffer[buffer_index] == OP_RIGHT_BRACKET){
                    count++;
                }else if(buffer[buffer_index] == OP_LEFT_BRACKET){
                    count--;
                }
                buffer_index--;
            }
        }
    }
}
